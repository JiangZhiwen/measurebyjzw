package wireless.measurelayer;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Button m1 = (Button)findViewById(R.id.type1);
        Button m2 = (Button)findViewById(R.id.type2);
        Button m3 = (Button)findViewById(R.id.type3);
        m1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeasureLayer test = (MeasureLayer)findViewById(R.id.measureLayer);
                test.setModeandType(1,"picPath");
            }
        });
        m2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeasureLayer test = (MeasureLayer)findViewById(R.id.measureLayer);
                test.setModeandType(2,"picPath");
            }
        });
        m3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeasureLayer test = (MeasureLayer)findViewById(R.id.measureLayer);
                test.setModeandType(3,"picPath");
            }
        });
    }
}
