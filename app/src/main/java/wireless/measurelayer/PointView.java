package wireless.measurelayer;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by jiangzhiwen on 15/11/23.
 */
public class PointView extends ImageView {
    private String TypeName;
    static int POINT_WIDTH=100;
    static int POINT_HEIGHT=100;

    public PointView(Context context){
        super(context);
    }
    public PointView(Context context,AttributeSet attrs){
        super(context,attrs);
    }
    public void setType(String typename){
        TypeName = typename;
    }
    public void setHighlight(){
        TypeName = TypeName+"_hightlight";
        this.setBackgroundColor(Color.parseColor("#F5F5DC"));
    }
    public void setNormalStyle(){
        //set background to normal style
        this.setBackgroundColor(Color.parseColor("#000000"));

    }
    public boolean isInside(float x, float y){
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
        if (Math.round(x)>layoutParams.leftMargin&&Math.round(x)<layoutParams.leftMargin+POINT_WIDTH
                &&Math.round(y)>layoutParams.topMargin&&Math.round(y)<layoutParams.topMargin+POINT_HEIGHT){
            return true;
        }
        return false;
    }
//    @Override
//    public boolean onTouchEvent(MotionEvent event){
//        this.setBackgroundColor(Color.parseColor("#F5F5DC"));
//        switch (event.getAction() & MotionEvent.ACTION_MASK) {
//            case MotionEvent.ACTION_DOWN:
////                setLayoutParams(new AbsoluteLayout.LayoutParams(100, 100, Math.round(event.getX()), Math.round(event.getY())));
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//            case MotionEvent.ACTION_POINTER_DOWN:
//                break;
//            case MotionEvent.ACTION_POINTER_UP:
//                break;
//            case MotionEvent.ACTION_MOVE:
//                Log.d("moving", "moving now");
////                setLayoutParams(new AbsoluteLayout.LayoutParams(100, 100,Math.round(event.getX()),Math.round(event.getY())));
//                break;
//        }
//        _root.invalidate();
//        invalidate();
//        Log.d("onclick", "have red inside");         return true;
//        return true;
//    }


}
