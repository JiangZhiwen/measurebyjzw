package wireless.measurelayer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import wireless.measurelayer.PointView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jiangzhiwen on 15/11/23.
 */
public class MeasureLayer extends RelativeLayout {
    private int MeasureMode;
    private int totalPointCount;
    private String PointType;
//    int test = 0;
    private List<PointView> PointList = new ArrayList<PointView>();
    PointView TempPoint = null;
    private PointView CurrentPoint;
    private boolean Measuring = false;
    private float centerPoint_X;
    private float centerPoint_Y;
    private boolean IsMiddlePointTouched = false;
    private boolean FirstDrawing = true;
    private double distance = 0;
    public MeasureLayer(Context context){
        super(context);
        this.setWillNotDraw(false);
    }
    public MeasureLayer(Context context,AttributeSet attrs){
        super(context, attrs);
        this.setWillNotDraw(false);
    }

    public void setModeandType(int measuremode,String pointtype){
        MeasureMode=measuremode;   //1.line(one point) 2.line(two points) 3.oval(three points) 4.heartbeats measure(two points)
        PointType = pointtype;  //point type is the background image name
    }
    public PointView getCurrentPoint(){
        CurrentPoint = new PointView(getContext());
        return CurrentPoint;
    }
    public void cleanHighlight(){
        //clean all the highlight mode
        for (int i = 0; i< PointList.size();i++){
            PointView temp = PointList.get(i);
            temp.setNormalStyle();
        }
    }
    //Point moved by five direction arrow
    public void PointMove(){

    }
    public void setCenterPoint(float x,float y){
        centerPoint_X = x;
        centerPoint_Y = y;
    }
    private void addPoint(float x, float y){
        TempPoint = null;
        PointView temp = new PointView(getContext());
        temp.setBackgroundColor(Color.parseColor("#000000"));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(PointView.POINT_WIDTH, PointView.POINT_HEIGHT);
        layoutParams.leftMargin = Math.round(x)-PointView.POINT_WIDTH/2;
        layoutParams.topMargin = Math.round(y)-PointView.POINT_HEIGHT/2;
        layoutParams.bottomMargin = -250;
        layoutParams.rightMargin = -250;
        temp.setLayoutParams(layoutParams);
        this.addView(temp);
        PointList.add(temp);
        Log.d("touching", "add Point");
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
//        invalidate();
        boolean isTouchPoint = false;
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                for (int i = 0;i<PointList.size();i++){
                    if (PointList.get(i).isInside(event.getX(),event.getY())){
                        isTouchPoint = true;
                        TempPoint = PointList.get(i);
                        cleanHighlight();
                        TempPoint.setHighlight();
                        if (i==2){
                            Log.d("MIDDLE CLICKED","TRUE");
                            IsMiddlePointTouched = true;
                        }else{
                            IsMiddlePointTouched = false;
                        }
                        Log.d("touching","touch inside point");
                    }
                }
                if (!isTouchPoint){
                    switch (MeasureMode){
                        case 1:
                            if (PointList.size()<1)
                                addPoint(event.getX(),event.getY());
                            if (PointList.size()==1)
                                Measuring = true;
                                invalidate();//绘制射线
                            break;
                        case 2:
                            if (PointList.size()<2)
                                addPoint(event.getX(),event.getY());
                            if (PointList.size()==2)
                                Measuring = true;
                                invalidate();//绘制两点间连线
                            break;
                        case 3:
                            if (PointList.size()<3)
                                addPoint(event.getX(),event.getY());
                            if (PointList.size()==3)
                                Measuring = true;
                                this.invalidate();//绘制椭圆
                            break;
                        case 4:
                            if (PointList.size()<2)
                                addPoint(event.getX(),event.getY());
                            if (PointList.size()==2)
                                Measuring = true;
                                invalidate();//绘制测心率线
                            break;
                    }
                }
//                break;
            case MotionEvent.ACTION_UP:
//                IsMiddlePointTouched = false;
                if (MeasureMode==3&&PointList.size()==3){
                    placeMiddlePoint();
                    this.invalidate();
                }
                Log.d("MIDDLE CLICKED","FALSE");
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
//                Log.d("moving","parents moving now");
                if (TempPoint!=null){
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) TempPoint.getLayoutParams();
                    layoutParams.leftMargin = Math.round(event.getX()-PointView.POINT_WIDTH/2);
                    layoutParams.topMargin = Math.round(event.getY()-PointView.POINT_HEIGHT/2);
                    layoutParams.rightMargin = -250;
                    layoutParams.bottomMargin = -250;
                    TempPoint.setLayoutParams(layoutParams);
//                    this.invalidate();
                    Log.d("touching", "point Moving");
                }
                break;
        }

//        Log.d("ontouch","have touched inside");
        return true;

    }
    //drawing
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (Measuring){
            if (MeasureMode==1){
                drawLine(canvas,0);
            }else if(MeasureMode==2){
                drawLine(canvas,1);
            }else if(MeasureMode==3){
                Log.d("~~~~~~~","DRAWING OVAL");
                drawOval(canvas);
            }else{  //MeasureMode = 4

            }
        }

//        test = test+10;
    }
    private void drawLine(Canvas canvas,int type){
        float fromX;
        float fromY;
        float toX;
        float toY;
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        if (type==0){
            fromX = width/2;
            fromY = -50;
            //fromX = centerPoint_X;
            //fromY = centerPoint_Y;
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) PointList.get(0).getLayoutParams();
            if (fromX==layoutParams.leftMargin+PointView.POINT_WIDTH/2){
                //边界处理
                toX = fromX;
                toY = height;
            }else{
                float a,b;
                float tempX = layoutParams.leftMargin+PointView.POINT_WIDTH/2;
                float tempY = layoutParams.topMargin+PointView.POINT_HEIGHT/2;
                a = (fromY - tempY)/(fromX - tempX);
                b = (tempY*fromX - tempX*fromY)/(fromX - tempX);
                toX = (height - b)/a;
                toY = height;
            }
        }else{
            fromX = ((LayoutParams) PointList.get(0).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
            fromY = ((LayoutParams) PointList.get(0).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
            toX = ((LayoutParams) PointList.get(1).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
            toY = ((LayoutParams) PointList.get(1).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        }
        Paint paint = new Paint();
        paint.setColor(Color.WHITE); //设置画笔颜色 
        canvas.drawColor(Color.TRANSPARENT);//设置背景颜色 
        paint.setShadowLayer(10, 5, 5, Color.BLACK);
        paint.setStrokeWidth((float) 3.0);//设置线宽 
        canvas.drawLine(fromX, fromY, toX, toY, paint);//绘制直线 from xy to xy use paint
    }
    private void drawOval(Canvas canvas){
        Log.d("XXXXXXXXXX","DRAWING!!!!!!!!!!!!!");
        Paint paint = new Paint();
        paint.setColor(Color.WHITE); //设置画笔颜色 
        canvas.drawColor(Color.TRANSPARENT);//设置背景颜色 
        paint.setShadowLayer(10, 5, 5, Color.BLACK);
        paint.setStrokeWidth((float) 3.0);//设置线宽 
        float firstPoint_X = ((LayoutParams) PointList.get(0).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float firstPoint_Y = ((LayoutParams) PointList.get(0).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        float lastPoint_X = ((LayoutParams) PointList.get(1).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float lastPoint_Y = ((LayoutParams) PointList.get(1).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        float middlePoint_X = ((LayoutParams) PointList.get(2).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float middlePoint_Y = ((LayoutParams) PointList.get(2).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        PointView middlePoint = PointList.get(2);
        double a, b;
        double length = Math.sqrt((lastPoint_X - firstPoint_X) * (lastPoint_X - firstPoint_X) + (lastPoint_Y - firstPoint_Y) * (lastPoint_Y - firstPoint_Y))/ 2;
        double Xlength = lastPoint_X - firstPoint_X;
        double Ylength = lastPoint_Y - firstPoint_Y;
        double angle = Math.atan(Ylength / Xlength);
        double halfX = (firstPoint_X + lastPoint_X) / 2;
        double halfY = (firstPoint_Y + lastPoint_Y) / 2;
        if (FirstDrawing||IsMiddlePointTouched){
//        if(true){
            if (lastPoint_X == firstPoint_X)
            {
                distance = Math.abs(middlePoint_X - firstPoint_X);
            } else
            {
                a = (lastPoint_Y - firstPoint_Y) / (lastPoint_X - firstPoint_X);
                b = (firstPoint_Y * lastPoint_X - firstPoint_X * lastPoint_Y) / (lastPoint_X - firstPoint_X);
                //move middle point
//            CGPoint middlePoint = CGPointMake(self.middlePoint.x, self.middlePoint.y);
                distance = Math.abs((a * middlePoint_X - middlePoint_Y + b) / Math.sqrt(a * a + 1));
            }
            FirstDrawing = false;

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) middlePoint.getLayoutParams();
            layoutParams.leftMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.cos(angle) - distance * Math.sin(-Math.PI / 2) * Math.sin(angle) + halfX-PointView.POINT_WIDTH/2);
            layoutParams.topMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.sin(angle) + distance * Math.sin(-Math.PI / 2) * Math.cos(angle) + halfY-PointView.POINT_HEIGHT/2);
            layoutParams.rightMargin = -250;
            layoutParams.bottomMargin = -250;
//            middlePoint.setLayoutParams(layoutParams);
        }

////////////

        double fromX = lastPoint_X;
        double fromY = lastPoint_Y;
        double toX;
        double toY;
        for (double ti = 0.0; ti < 2 * Math.PI; ti += Math.PI / 100)
        {
            toX = length * Math.cos(ti) * Math.cos(angle) - distance * Math.sin(ti) * Math.sin(angle) + halfX;
            toY = length * Math.cos(ti) * Math.sin(angle) + distance * Math.sin(ti) * Math.cos(angle) + halfY;
            canvas.drawLine((float)fromX, (float)fromY, (float)toX, (float)toY, paint);
            fromX = toX;
            fromY = toY;
        }
        //movePoint();
        Log.d("!!!!!!!!!!!!!!!!!", "MOVING~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) middlePoint.getLayoutParams();
        layoutParams.leftMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.cos(angle) - distance * Math.sin(-Math.PI / 2) * Math.sin(angle) + halfX-PointView.POINT_WIDTH/2);
        layoutParams.topMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.sin(angle) + distance * Math.sin(-Math.PI / 2) * Math.cos(angle) + halfY-PointView.POINT_HEIGHT/2);
        layoutParams.rightMargin = -250;
        layoutParams.bottomMargin = -250;
//        middlePoint.setLayoutParams(layoutParams);
    }
    private void placeMiddlePoint(){
        float firstPoint_X = ((LayoutParams) PointList.get(0).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float firstPoint_Y = ((LayoutParams) PointList.get(0).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        float lastPoint_X = ((LayoutParams) PointList.get(1).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float lastPoint_Y = ((LayoutParams) PointList.get(1).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        float middlePoint_X = ((LayoutParams) PointList.get(2).getLayoutParams()).leftMargin+PointView.POINT_WIDTH/2;
        float middlePoint_Y = ((LayoutParams) PointList.get(2).getLayoutParams()).topMargin+PointView.POINT_HEIGHT/2;
        PointView middlePoint = PointList.get(2);
        double a, b;
        double length = Math.sqrt((lastPoint_X - firstPoint_X) * (lastPoint_X - firstPoint_X) + (lastPoint_Y - firstPoint_Y) * (lastPoint_Y - firstPoint_Y))/ 2;
        double Xlength = lastPoint_X - firstPoint_X;
        double Ylength = lastPoint_Y - firstPoint_Y;
        double angle = Math.atan(Ylength / Xlength);
        double halfX = (firstPoint_X + lastPoint_X) / 2;
        double halfY = (firstPoint_Y + lastPoint_Y) / 2;
        if (lastPoint_X == firstPoint_X)
        {
            distance = Math.abs(middlePoint_X - firstPoint_X);
        } else
        {
            a = (lastPoint_Y - firstPoint_Y) / (lastPoint_X - firstPoint_X);
            b = (firstPoint_Y * lastPoint_X - firstPoint_X * lastPoint_Y) / (lastPoint_X - firstPoint_X);
            //move middle point
//            CGPoint middlePoint = CGPointMake(self.middlePoint.x, self.middlePoint.y);
            distance = Math.abs((a * middlePoint_X - middlePoint_Y + b) / Math.sqrt(a * a + 1));
        }
        FirstDrawing = false;

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) middlePoint.getLayoutParams();
        layoutParams.leftMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.cos(angle) - distance * Math.sin(-Math.PI / 2) * Math.sin(angle) + halfX-PointView.POINT_WIDTH/2);
        layoutParams.topMargin = (int)(length * Math.cos(-Math.PI / 2) * Math.sin(angle) + distance * Math.sin(-Math.PI / 2) * Math.cos(angle) + halfY-PointView.POINT_HEIGHT/2);
        layoutParams.rightMargin = -250;
        layoutParams.bottomMargin = -250;
    }
    //heart beats lines
    private void drawLines(Canvas canvas){

    }

}
